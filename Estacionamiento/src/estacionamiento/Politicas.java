package estacionamiento;

import java.util.ArrayList;
import java.util.Random;

public class Politicas {
	private static Politicas unique;
	
	private static Matriz TenEspera;
	private static int[] prioridades;
	
	private Politicas(int[] prioridades)
	{
		Politicas.prioridades=prioridades;
	}
	
	public static Politicas getInstance(int[] prioridades)
	{
		if(unique==null)unique=new Politicas(prioridades);
		return unique;
	}
	
	public static int cual()//Devuelve el N° de una trans sensibil y en cola con mayor prioridad
	//En caso de haber mas de una trans con igual prioridad,sensibiliz y en cola, la selección es pseudo-aleatoria
	{
		TenEspera=RedDePetri.getSensibilizadas().and(Colas.quienesEstan());
		ArrayList <Integer>TenEspera_EqualPrio=new ArrayList<Integer>();
		int prioridad=-1,Ntrans=0,i;
		Random r = new Random();
		for(i=0;i<TenEspera.getNfilas();i++)
		{
			if(TenEspera.get()[i][0]==1 && prioridades[i]>prioridad) {
				Ntrans=i;prioridad=prioridades[i];
				TenEspera_EqualPrio.clear();
					}
			else if (TenEspera.get()[i][0]==1 && prioridades[i]==prioridad) {
				if(TenEspera_EqualPrio.size()==0)TenEspera_EqualPrio.add(Ntrans);
				TenEspera_EqualPrio.add(i);}
		}
		if (TenEspera_EqualPrio.size()>1) 
			return TenEspera_EqualPrio.get(r.nextInt(TenEspera_EqualPrio.size()));
		else if (i>0)return Ntrans;
			throw new RuntimeException("No hay transiciones sensibilizadas y en cola");
	}
	
	public static int[] getPrioridades()
	{
		return prioridades;
	}
}