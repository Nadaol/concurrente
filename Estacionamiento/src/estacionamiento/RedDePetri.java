package estacionamiento;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Random;

public class RedDePetri {
	
	//Variables de control
	
	private static int Nshots;//N disparos totales
	private static boolean printPinv=false;
	private static boolean finished=false;
	
	//Variables estadisticas
	
	private static long time;//tiempo de ejecucion
	private static ArrayList<Integer> firings;//Log de disparos
	private static int [] Tshots;//N de disparos por transicion
	private static ArrayList<ArrayList<Integer>> Tinv_notFinished;//Tinvariantes no completados
	private static int[] Cycles;//Tinvariantes completados
	private static int[] Ttimes;//tiempos promedio (ms) de disparo de cada transicion temporizada
	
	
	//Variables de definicion de estructura de la red
	
	private static RedDePetri unique;//Objeto,esta red de petri (singleton)
	private static Matriz Incidencia_Anterior;//I+ (Filas->Plazas,Columnas->Transiciones)
	private static Matriz Incidencia_Posterior;//I-
	private static Matriz Incidencia;//I = (I+)-(I-) 
	private static Matriz Incidencia_Inhibicion;//Matriz de arcos inhibidores (H) (Filas->Transiciones,Columnas->Plazas)
	private static Matriz Incidencia_lector;//Matriz de arcos lectores (R)
	private static Matriz Intervalos;//Tiempos de sensibilizacion 
	private static String [] Transiciones_info;//Nombres-info de las trancisiones
	private static String [] Plazas_info;//Nombres-info de las plazas
	private static int[][] Pinvariants;//matriz binaria de Pinvariantes
	private static int[] Pvalues;//valor de los pinvariantes
	private static ArrayList<Integer>[] Tinvariants;
	
	
    //Variables dinamicas del sistema
	
	private static int[] Marcado;//Marcado actual de la red de petri
	private static int[] Sensibilizadas_inhibidas;//Vector de sensibilizacion por arcos inhibidores (B)
	private static int[] Sensibilizadas_regulares;//Vector de sensibilizacion por arcos regulares (E)
	private static int[] Sensibilizadas_lector;//Vector de sensibilizacion por arcos lectores (L)
	public static long[][] Vector_contador;//Vector contador de tiempo(ms),desde que una transicion temporal se sensibiliza
	private static int[] Sensibilizadas;//Vector de transiciones sensibilizadas extendida(incluye arcos inhibidores,lectores y regulares) (Ex)
	
	
	// ----------------------------  Instanciacion de la red  -----------------------------------------------------

	//Constructor privado(singleton) de la red de petri
	private RedDePetri(Matriz inc_ant,Matriz inc_pos,Matriz inc_inhib,Matriz Inc_lector,int[] marcado_inicial,
			Matriz intervalos,String [] pinfo,String [] tinfo,
			int [][] Pinvariants,int[] Pvalues,ArrayList<Integer>[]Tinvariants,boolean pinv,int Nshots) throws RuntimeException {
		
		//Cargado e impresion de datos de definicion de la red
		
		printPinv=pinv;
		Incidencia_Anterior=inc_ant;
		Incidencia_Posterior=inc_pos;
		RedDePetri.Nshots=Nshots;

		Cycles = new int[Tinvariants.length];
		Ttimes = new int[inc_pos.getNcolumnas()];
		Tinv_notFinished=new ArrayList<ArrayList<Integer>>();
		
		Log.prints("Matriz Incidencia Anterior cargada exitosamente\n");
		Incidencia_Anterior.imprimir();
		
		Log.prints("Matriz Incidencia Posterior cargada exitosamente\n");
		Incidencia_Posterior.imprimir();
		
		Incidencia=Incidencia_Posterior.restar(Incidencia_Anterior);
		Log.prints("Matriz Incidencia calculada exitosamente\n");
		Incidencia.imprimir();
		
		if(inc_inhib.getNfilas()!=Incidencia.getNcolumnas() || inc_inhib.getNcolumnas()!=Incidencia.getNfilas())throw new RuntimeException("Matriz de inhibicion invalida");
		Incidencia_Inhibicion=inc_inhib;
		
		Log.prints("Matriz de Inhibicion cargada exitosamente\n");
		Incidencia_Inhibicion.imprimir();
		
		Inc_lector=Inc_lector.getTranspuesta();
		if(Inc_lector.getNfilas()!=Incidencia.getNcolumnas() || Inc_lector.getNcolumnas()!=Incidencia.getNfilas())throw new RuntimeException("Matriz de inhibicion invalida");
		Incidencia_lector=Inc_lector;
		
		Log.prints("Matriz Incidencia Lector cargada exitosamente\n");
		Incidencia_lector.imprimir();
		
		if(marcado_inicial.length!=Incidencia.getNfilas())throw new RuntimeException("Vector de marcado inicial invalido");
		Marcado=marcado_inicial;
		
		Log.prints("Marcado inicial cargado exitosamente\n");
		print(Marcado);
				
		if(intervalos.getNfilas()!=Incidencia.getNcolumnas() &&intervalos.getNcolumnas()!=2)throw new RuntimeException("Vector de intervalos invalido");
		Intervalos=intervalos;
		
		Log.prints("Intervalos cargados exitosamente\n");
		Intervalos.getTranspuesta().imprimir();
		
		if(pinfo.length!=Incidencia.getNfilas()||tinfo.length!=Incidencia.getNcolumnas())throw new RuntimeException("Info invalida");
		Plazas_info=pinfo;
		Transiciones_info=tinfo;
		
		Log.prints("Nombres de plazas cargados exitosamente\n\n");
		print(Plazas_info);
		Log.prints("Nombres de transisiones cargados exitosamente\n\n");
		print(Transiciones_info);
		
		Log.prints("Prioridades\n");
		print(Politicas.getPrioridades());
		
		if(Pinvariants[0].length!=Incidencia.getNfilas())throw new RuntimeException("Matriz de P-invariantes invalida");
		RedDePetri.Pinvariants=Pinvariants;
		
		Log.prints("Invariantes de plaza cargados exitosamente\n");
		Matriz pinvariants = new Matriz(RedDePetri.Pinvariants);
		pinvariants.imprimir();
		
		RedDePetri.Pvalues=Pvalues;
		
		Log.prints("Valores de invariante de plazas cargados\n");
		Matriz pvalues = new Matriz(RedDePetri.Pvalues);
		pvalues.getTranspuesta().imprimir();
		
		RedDePetri.Tinvariants=Tinvariants;
		Log.prints("Invariantes de transicion cargados exitosamente\n");
		for(int i=0;i<Tinvariants.length;i++)print(Tinvariants[i]);
		Log.prints("\n\n");
		
		firings=new ArrayList<Integer>();
		Tshots=new int[Incidencia.getNcolumnas()];
		
		
		Sensibilizadas_regulares=new int[Incidencia.getNcolumnas()];
		Sensibilizadas_inhibidas=new int[Incidencia.getNcolumnas()];
		Sensibilizadas_lector=new int[Incidencia.getNcolumnas()];;
		Vector_contador=new long[Incidencia.getNcolumnas()][2];
		Sensibilizadas=new int[Incidencia.getNcolumnas()];
		Update_Sensibilizadas();//calculo de sensibilizadas iniciales
		
		//impresion del tiempo de creación
		time = System.currentTimeMillis();
		Log.prints("Red de petri creada correctamente : " + new Timestamp(time) + "\n\n\n");
		
	}
	
	//Creacion/obtencion de la red de petri
	public static RedDePetri getInstance(Matriz inc_ant,Matriz inc_pos,Matriz inc_inhib,Matriz Inc_lector,int[] marcado_inicial,
			Matriz intervalos,String [] pinfo,String [] tinfo,
			int[][] Pinvariants,int[] Pvalues,ArrayList<Integer>[]Tinvariants,boolean pinv,int Nshots) throws RuntimeException {
		if(unique == null) {
			unique = new RedDePetri(inc_ant,inc_pos,inc_inhib,Inc_lector,marcado_inicial,intervalos,
					pinfo,tinfo,Pinvariants,Pvalues,Tinvariants,pinv,Nshots);
		}
		return unique;
	}
	
	
	// -------------------------------- Calculo de sensibilizadas y marcado ------------------------------------------------
	
	
	//Actualizo el vector binario de sensibilizacion de arcos regulares
	private static void Update_Sensibilizadas_regulares()
	{
		for(int i=0;i<Incidencia.getNcolumnas();i++) {
			Sensibilizadas_regulares[i]=Sign(getMarcado(i));//retorna 1 si el marcado de disparo i no tiene valores nulos 
		}
	}
	
	//Actualizo vector binario de sensibilizacion de arcos inhibidores
	private static void Update_Sensibilizadas_inhibidas()
	{//Multiplicacion matriz de inhibicion con el vector de inhibicion (B=H*Q)
		//transicion no sensibilizada si existe un arco inhibidor y el elemento del vector es nulo (hay tokens)
		Sensibilizadas_inhibidas=Incidencia_Inhibicion.multiplicar2(getInhibitionVector()).getColumna(0);
	}
	
	//Actualizo vector binario de sensibilizacion de arcos inhibidores
	private static void Update_Sensibilizadas_lector()
	{//Multiplicacion matriz de inhibicion con el vector de inhibicion (B=H*Q)
		//transicion no sensibilizada si existe un arco lector y el elemento del vector es nulo (no hay tokens)
		Sensibilizadas_lector=Incidencia_lector.multiplicar2(getLectorVector()).getColumna(0);
	}
	
	//Actualizo vector binario de sensibilizacion extendido 
	private static void Update_Sensibilizadas()
	{
		Update_Sensibilizadas_regulares();//actualizo las sensibilizaciones de todos los arcos
		Update_Sensibilizadas_inhibidas();
		Update_Sensibilizadas_lector();
		Update_contador();//actualizo el contador de tiempos de sensibilizacion
		for(int i=0;i<Incidencia.getNcolumnas();i++) {
			//multiplicacion binaria (Ex = E and B and Z)
Sensibilizadas[i]=Sensibilizadas_regulares[i]*Sensibilizadas_inhibidas[i]*Sensibilizadas_lector[i];
				}
	}

	
	//Devuelvo un vector binario,si el marcado es nulo -> 1 sino 0
	private static Matriz getInhibitionVector()
	{
		int[] inhib_vec = new int[Incidencia.getNfilas()];
		for(int i=0;i<Incidencia.getNfilas();i++)
			{
			if(Marcado[i]==0)inhib_vec[i]=1;
			else inhib_vec[i]=0;
			}
		return new Matriz(inhib_vec);
	}
	
	//Idem inhibition vector pero inverso
	private static Matriz getLectorVector()
	{
		int[] lec_vec = new int[Incidencia.getNfilas()];
		for(int i=0;i<Incidencia.getNfilas();i++)
			{
			if(Marcado[i]==0)lec_vec[i]=0;
			else lec_vec[i]=1;
			}
		return new Matriz(lec_vec);
	}
	
	
	//Checkea que las componentes del vector sean todas mayor o igual a 0
	private static int Sign(int[] vector)
	{
		for(int i=0;i<vector.length;i++)if(vector[i]<0)return 0;
		return 1;
	}
	
	//Devuelve el posible marcado si la transicion Ntrans se disparara
	private static int[] getMarcado(int Ntrans)
	{
		if(Ntrans<0 || Ntrans>Incidencia.getNcolumnas())throw new RuntimeException("Numero de transicion invalido");
		int[] marcado_aux=Incidencia.getColumna(Ntrans);
		for(int i=0;i<Incidencia.getNfilas();i++)marcado_aux[i]+=Marcado[i];
		return marcado_aux;
	}
	
	
	
	// ------------------------- Calculos para transiciones temporales ------------------------
	
	
	//acualizo vector de tiempo transcurrido desde la sensibilizacion de las transiciones con tiempo	
	private static void Update_contador()
	{
	for(int i=0;i<Incidencia.getNcolumnas();i++) {		
		if(Intervalos.get()[i][0]>0) {
			//si la transicion es con tiempo (limite inferior>0)
			long tiempo_actual = System.currentTimeMillis();
			
			if(Sensibilizadas_regulares[i]==1 && Sensibilizadas_inhibidas[i]==1 && Sensibilizadas_lector[i]==1)
			{
				//si la transicion está sensibilizada
				
				if(Vector_contador[i][0]==0)Vector_contador[i][0]=tiempo_actual;//si recien se sensibilizo seteo el timestamp inicial
				Vector_contador[i][1]=tiempo_actual-Vector_contador[i][0];//si no resto el tiempo actual con el timestamp inicial
			}
			else {Ttimes[i]+=Vector_contador[i][1];resetTime(i);}//si no esta sensibilizada reseteo contador y timestamp
		}	
			
	}
	 }
	
	public static long getVentana(int Ntrans) {
		
		/*devuelve : 0 -> si la transicion es automatica o con tiempo dentro del intervalo (lista para disparar)
		el tiempo en milisegundos restantes -> si no se cumplio el tiempo de sesibilizacion 
		-1 si el tiempo se paso del limite superior
*/
		
		//Si la Trans no es con tiempo retorno 0
		if(Intervalos.get()[Ntrans][0]==0 && Intervalos.get()[Ntrans][1]==0)return 0;
		Update_contador();//actualizo current
		long alfa=Intervalos.get()[Ntrans][0];
		long beta=Intervalos.get()[Ntrans][1];
		long current=Vector_contador[Ntrans][1];
		//Si el tiempo transcurrido esta dentro del intervalo retorno 0
		if(current>alfa && current<beta)return 0;
		
		else if(current<=alfa)
			{
				return alfa-current;
			}
		else return -1;//si el tiempo se excedio el beta
	}
	
	private static void resetTime(int Ntrans)
	{
		Vector_contador[Ntrans][0]=0;
		Vector_contador[Ntrans][1]=0;
	}
	
	public static boolean Temporal_Sensibilizada(int Ntrans)
	{//devuelve true si la trans es temporal y esta sesibilizada para el uso en el monitor
		if(Intervalos.get()[Ntrans][0]>0 && Sensibilizadas_regulares[Ntrans]==1 && 
				Sensibilizadas_inhibidas[Ntrans]==1 && Sensibilizadas_lector[Ntrans]==1)
			{
				return true;
			}
		else return false;
	}

	
	
//	----------------------------- Funciones de impresion de informacion de la red ----------------------------------------------
	

	//función privada para la impresion de informacion al disparar
		private static void print_firingInfo(int Ntrans)
		{
				String marcado="";
				String sensib="";
				String cola="";
				for(int i=0;i<Incidencia.getNfilas();i++)marcado+=(Marcado[i]+" ");
				for(int i=0;i<Incidencia.getNcolumnas();i++)if(Sensibilizadas[i]!=0)sensib+=(i+" ");
				for(int i=0;i<Incidencia.getNcolumnas();i++)if(Colas.quienesEstan().getColumna(0)[i]!=0)cola+=(i+" ");
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				Log.prints(String.format("Marcado Actual : %-55s \nSensibilizadas : %-40sEn Cola : %-40s \n%-15s disparo N° : %-9d (%d)%-55s%-28s \n\n",marcado,sensib,cola,Thread.currentThread().getName(),
						firings.size() ,Ntrans,Transiciones_info[Ntrans],timestamp.toString()));
		}
		
		//Impresion del total de disparos por transicion
		public static void printTshots()
		{
			Log.prints("----- Transiciones Disparadas -----\n\n");
			for(int i = 0; i < Tshots.length; i++) {
				Log.prints(String.format("T%d (%s): %d\n",i,Transiciones_info[i],Tshots[i]));
			}	
			Log.prints("\n\n");
		}
		
		//Impresion del tiempo promedio de disparo desde la sensibilizacion de las transiciones temporales
		public static void printAvgTimes()
		{
			Log.prints("----- Tiempo promedio de disparos temporizados -----\n\n");
			for(int i = 0; i < Ttimes.length; i++) {
				if(Ttimes[i]>0) {
					Log.prints(String.format("T%d (%s): %d\n",i,Transiciones_info[i],Ttimes[i]));}
			}	
			Log.prints("\n\n");
		}
		
		
		
//		----------------------- Funciones privadas para la impresion de arrays -------------------------
		
		
		//funcion privada para la impresion de vectores de integer's 
		private static void print(int[] vector)
		{
			Log.prints("\n");
			for(int i=0;i<vector.length;i++) {
				
				Log.prints(vector[i]+" ");
			}
			Log.prints("\n\n");
		}
		
		//funcion privada para la impresion de vectores de strings
		private static void print(String[] vector)
		{
			Log.prints("\n");
			for(int i=0;i<vector.length;i++) {
				
				Log.prints(vector[i]+"\n");
			}
			Log.prints("\n\n");
		}
		
		//funcion privada para la impresion de vectores de strings
		private static void print(ArrayList<Integer> vector)
		{
			Log.prints("\n");
			for(int i=0;i<vector.size();i++) {
				
				Log.prints(vector.get(i).toString()+" ");
			}
		}
	
	
	// ---------------------------------- Funciones para el analisis de la red ---------------------------------
	
	private static String checkPinvariants() {
		//Funcion privada para la comprobacion de los pinvariantes
		int sum;
		String Pinv="";
		for(int i=0;i<Pinvariants.length;i++)
		{
			sum=0;
		for(int j=0;j<Pinvariants[0].length;j++)
		{
			if(Pinvariants[i][j]==1) {	//si la plaza conforma el pinvariante
				Pinv+="P"+ getPnumber(Lector.getPinvInfo().get(j)) + "("+Marcado[getPnumber(Lector.getPinvInfo().get(j))]+")+";
				sum+=Marcado[getPnumber(Lector.getPinvInfo().get(j))];//suma el marcado actual de esa plaza a sum
			}
		
		}
		Pinv=Pinv.substring(0, Pinv.length() - 1);
		Pinv+="= "+sum+"\n";
		if(sum!=Pvalues[i])throw new RuntimeException("Pinvariants do not match!");//si sum no coincide con el valor leido del invariante
		}
return Pinv;
	}
	
	private static int getPnumber(String Pinfo)
	{//funcion para obtener el numero de la plaza con el nombre Pinfo
		for(int i=0;i<Plazas_info.length;i++)
		{
			if(Pinfo.equals(Plazas_info[i]))return i;
		}
		return -1;
	}
	
	public static void checkTinvariants ()
	{		
		ArrayList<Integer> Tshot_sequence=new ArrayList<Integer>();//transiciones equivalentes al Nshot_sequence
		ArrayList<Integer> Nshot_sequence=new ArrayList<Integer>();//Num de disparos q coinciden con una tinvariante(al completarla se eliminan de firings)
		int [] NTinv=new int[Tinvariants.length];
		int [] NTinv_Aux=new int[Tinvariants.length];//numero de invariantes q coinciden con la secuencia
		
		int k=0;
		boolean validShot=false;//si el disparo coincide con algun tinvariante valido
		
		for(int i=0;i<firings.size();i++)// i : iteracion de la secuencia de disparos (si sale del loop la invariante nunca termino)
			{
			
			for(int j=0;j<Tinvariants.length;j++) // j : iteracion de las Tinvariantes
			{
				if(Tinvariants[j].size()>k && Tinvariants[j].get(k)==firings.get(i) )//si el disparo coincide con la invariante 
				{
					if(NTinv[j]!=-1)validShot=true;//algun tinvariante valido coincide con el disparo
					if(NTinv[j]>=0) {//y no se agrego a NTinv
						NTinv_Aux[j]=1;
					if(Nshot_sequence.size()==k) {Nshot_sequence.add(i);Tshot_sequence.add(firings.get(i));}//si el disparo no se agrego a la secuencia
					if(Nshot_sequence.size()==Tinvariants[j].size())//si se completo la invariante
					{
						Cycles[j]++;
						delete(firings,Nshot_sequence);//elimino disparos correspondientes al tinvariante
						checkTinvariants();return;//llamada recursiva con nueva secuencia de disparos
					}
					}
				}
				else NTinv_Aux[j]=-1;//si el tinvariante no coincide con firings
				
			}
				if(validShot) //si al menos una tinvariante coincide
				{k++;validShot=false;NTinv=NTinv_Aux.clone();} //copio Ntinv=Ntinv_aux
			}
		
//detecto invariante q no termino 
		Tinv_notFinished.add(Tshot_sequence);
		delete(firings,Nshot_sequence);
	if(!firings.isEmpty())	{checkTinvariants();return;}
		
	//si no queda ningun disparo restante en firings, imprimo resultados
	Log.prints("Tinavariantes completados \n");
	for(int h=0;h<Cycles.length;h++) 
	{
		print(Tinvariants[h]);
		Log.prints(" : "+Integer.toString(Cycles[h]));
	}
	Log.prints("\n\n");
	Tshots=new int[Incidencia.getNcolumnas()];
	Log.prints("Tinavariantes no completados \n\n");
	for(int l=0;l<Tinv_notFinished.size();l++) 
	{
		for(int m=0;m<Tinv_notFinished.get(l).size();m++)Log.prints(Tinv_notFinished.get(l).get(m)+" ");
		Log.prints("\n");
	}
	}
	
	
	private static void delete (ArrayList<Integer> vector,ArrayList<Integer> indexes)
	{
		for(int k=0;k<indexes.size();k++) {
			int index=indexes.get(k)-k;
			vector.remove(index);
		}	
	}
	
	
	//------------------------------------Getters-----------------------------------
	
	public static long getTiempoEjecucion()
	{
		return time;
	}
	
	public static boolean isFinished() {
		return finished;
	}

	
	public static Matriz getSensibilizadas()
	{
		return new Matriz(Sensibilizadas);
	}
	
	//----------------------------------Funciones de prueba--------------------------------------
	
	
	//Disparo una transicion random sensibilizada para testear la red con el main
			public static void disparo()
			{
				Update_Sensibilizadas();
				int Ntrans=0;
				Ntrans=getRandomTransition();
		print_firingInfo(Ntrans);
				Marcado=getMarcado(Ntrans);
				Update_Sensibilizadas();
				firings.add(Ntrans);
			}
	
	//devuelve el N° de una transicion sensibilizada al azar 
			private static int getRandomTransition() 
			{
				int [] sensibilizadas = new int[Incidencia.getNcolumnas()];//transiciones sensibilizadas
				int j = 0;
				
				for(int i=0;i<Incidencia.getNcolumnas();i++)
				{
					if(Sensibilizadas[i]>0)//recorro vector para saber que transiciones estan sensibilizadas
						{
						if(i==0||i==16)return i;//prioridad apagado-prendido del cartel luminoso
						sensibilizadas[j]=i;
						j++;
						}
						}
				if (j==0 ) throw new RuntimeException("Ninguna transicion sensibilizada : Deadlock Reached");
				Random r = new Random();
				return sensibilizadas[r.nextInt(j)];//devuelve el numero de la transicion random
				}
			
			
			
//			---------------------------------- Disparo de la red ----------------------------------------------------------------
			
	
			//Disparo una transicion determinada,develvo true si se pudo disparar, false en lo contrario
			public static boolean disparo(int Ntrans) {
				if(firings.size()==Nshots) {
					finished=true;time=System.currentTimeMillis()-time;getAverageTtimes();return false;}
				long ventana= getVentana(Ntrans);
				//me fijo si el N° de transicion es válido
				if(Ntrans<0 || Ntrans>Incidencia.getNcolumnas())throw new RuntimeException("Numero de transicion invalido");
				//si no está sensibilizada 
				if(Sensibilizadas[Ntrans]==0 )return false;
				
				//si esta sensibilizada y ventana es 0 (transicion automatica o con tiempo cumplido),disparo
				if(ventana==0) {
					String Pinv=checkPinvariants();
					if(printPinv) {
				System.out.printf("Invariantes de plaza : \n%s\n",Pinv);
				Log.prints(String.format("Invariantes de plaza : \n%s\n",Pinv));}
				print_firingInfo(Ntrans);	
				//Actualizo el marcado ,sensibilizadas ,variables estadisticas y reseteo el contador de tiempo
				Marcado=getMarcado(Ntrans);
				Update_Sensibilizadas()	;
				Ttimes[Ntrans]+=Vector_contador[Ntrans][1];
				resetTime(Ntrans);
				firings.add(Ntrans);
				Log.addfiring(Ntrans);
				Tshots[Ntrans]++;
				return true;
				}
				
				return false;//transicion con tiempo no cumplido
			}
			
			
			private static void getAverageTtimes()//division de arrays para obtener el promedio de disparo de las transiciones temporales
			{
				for(int i=0;i<Ttimes.length;i++)if(Tshots[i]>0)Ttimes[i]=Ttimes[i]/Tshots[i];else Ttimes[i]=0;
			}

}