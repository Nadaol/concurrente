package estacionamiento;

import java.util.concurrent.Semaphore;

public class Colas {
    private static Semaphore[] semaforos;  // semaforos de las transiciones
    private static int size;
    private static Colas unique;

    private Colas()
    {
    	size=RedDePetri.getSensibilizadas().getNfilas();
    	semaforos = new Semaphore[size] ;
          for(int i = 0; i < size; i++) {
              semaforos[i]= new Semaphore(0,true);
          }
    }
    
    public static Colas getInstance()
    {
    	if(unique==null)unique=new Colas();
    	return unique;
    }
    
    public static void releaseT(int NTrans)
    { 
    	if (semaforos[NTrans].availablePermits()==0) {semaforos[NTrans].release(); 
    //	System.out.println(Thread.currentThread().getName()+" Released T"+NTrans);
    	}
    }
    
    public static void releaseAll()
    { 
    	for(int i=0;i<size;i++) {
    		if (semaforos[i].hasQueuedThreads()&&semaforos[i].availablePermits()<=0) {semaforos[i].release();
    		}
    	}
    }
    
    public static void adquireT(int NTrans)
    {
    	  try {//System.out.println(Thread.currentThread().getName()+" Acquired T"+NTrans);
    		  if(RedDePetri.getSensibilizadas().getColumna(0)[NTrans]==0)semaforos[NTrans].acquire();
    		  }
	        catch (InterruptedException e) {
	            e.printStackTrace();
	            System.out.println("Error al entrar al semaforo T" + NTrans); }
    }
    
    public static boolean TryadquireT(int NTrans)
    {	
    		 if(semaforos[NTrans].tryAcquire()) {
    			 System.out.println(Thread.currentThread().getName()+" Acquired T"+NTrans);
    			 return true;
    		 }
    		 return false;
 }
    
    public static Matriz quienesEstan()
    {
    	int Vc []=new int [size];
    	 for(int i = 0; i < size; i++) {
             Vc[i]=semaforos[i].getQueueLength();
    }
    	 return new Matriz(Vc);
}
}