package estacionamiento;

public class Task implements Runnable{
	
	int[] Transiciones_Asignadas;

	public Task(int[] Transiciones)
	{
		if (Transiciones==null)throw new RuntimeException("Vector de transiciones nulo");
		Transiciones_Asignadas=Transiciones;
	}
	@Override
	public void run() {
		while(!RedDePetri.isFinished())
		for(int i=0;i<Transiciones_Asignadas.length && !RedDePetri.isFinished();i++)
			try {
				Monitor.Disparar(Transiciones_Asignadas[i]);//disparo las trancisiones asignadas hasta que la red finalice
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
	}

}