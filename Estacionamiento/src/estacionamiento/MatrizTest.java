package estacionamiento;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(OrderAnnotation.class)

class MatrizTest {

	@Test
	 @Order(1) 
	void test1_Carga_Archivo() throws Exception {
		Matriz A=new Matriz("/root/git/concurrente/Estacionamiento/src/Ia_Test.txt");
		assert(A.getNcolumnas()>0 && A.getNfilas()>0);
	}

	@Test
	 @Order(2) 
	void test2_Carga_Array1D() {
		int[] array= new int[4];
		array[0]=2;array[1]=0;array[2]=0;array[3]=0;
		Matriz B=new Matriz(array);
		assert(B.getNcolumnas()>0 && B.getNfilas()>0);
	}

	@Test
	 @Order(3) 
	void test3_Carga_Array2D() {
		int[][] array= new int[2][4];
		array[0][0]=1;array[0][1]=2;array[0][2]=3;array[0][3]=4;
		array[1][0]=5;array[1][1]=6;array[1][2]=7;array[1][3]=8;
		Matriz C=new Matriz(array);
		assert(C.getNcolumnas()>0 && C.getNfilas()>0);
	}

	@Test
	 @Order(4) 
	void test4_Suma() {
		Matriz C,D;
		int[][] array= new int[2][4];
		array[0][0]=1;array[0][1]=2;array[0][2]=3;array[0][3]=4;
		array[1][0]=5;array[1][1]=6;array[1][2]=7;array[1][3]=8;
		C=new Matriz(array);
		D=C.sumar(C);
		for(int i=0;i<D.getNfilas();i++)
			for(int j=0;j<D.getNcolumnas();j++)
			assertEquals(D.get()[i][j],C.get()[i][j]+C.get()[i][j]);
	}

	@Test
	 @Order(5) 
	void test5_Resta() {
		Matriz C,D;
		int[][] array= new int[2][4];
		array[0][0]=1;array[0][1]=2;array[0][2]=3;array[0][3]=4;
		array[1][0]=5;array[1][1]=6;array[1][2]=7;array[1][3]=8;
		C=new Matriz(array);
		D=C.restar(C);
		for(int i=0;i<D.getNfilas();i++)
			for(int j=0;j<D.getNcolumnas();j++)
			assertEquals(D.get()[i][j],0);
	}
	
	@Test
	 @Order(6) 
	void test6_Transpuesta() {
		Matriz C,D;
		int[][] array= new int[2][4];
		array[0][0]=1;array[0][1]=2;array[0][2]=3;array[0][3]=4;
		array[1][0]=5;array[1][1]=6;array[1][2]=7;array[1][3]=8;
		C=new Matriz(array);
		D=C.getTranspuesta();
		for(int i=0;i<D.getNfilas();i++)
			for(int j=0;j<D.getNcolumnas();j++)
			assertEquals(D.get()[i][j],C.get()[j][i]);
	}

	@Test
	 @Order(7) 
	void test7_multiplicar() {
		Matriz C,D;
		int[][] array= new int[2][2];
		array[0][0]=1;array[0][1]=2;
		array[1][0]=5;array[1][1]=6;
		C=new Matriz(array);
		D=C.multiplicar(C);
		assertEquals(D.get()[0][0],11);
		assertEquals(D.get()[0][1],14);
		assertEquals(D.get()[1][0],35);
		assertEquals(D.get()[1][1],46);

		}

	}

