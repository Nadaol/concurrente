package estacionamiento;

import java.awt.Cursor;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

public class Main {

	public static void main(String[] args) throws Exception {
		
		ArrayList<Thread> threads=new ArrayList<Thread>();
		
//		final String initial_path = "C:/Users/nadao/Desktop/git/concurrente/";
		final String initial_path = "/home/nadaol/git/concurrente/";	
		
		final String [] src_dir = new String [3];
		src_dir[0] = initial_path + "archivos_lectura/Prioridades_Indistinto/";//directorio fuente 
		src_dir[1] = initial_path + "archivos_lectura/Prioridades_Salida2/";//directorio fuente 
		src_dir[2] = initial_path + "archivos_lectura/Prioridades_PB/";//directorio fuente


        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        Font font = new Font("Arial", Font.BOLD, 11);
        
		int dir=-1;//eleccion del directorio para cargar la red
			
		    String[] choices = { "1)Prioridad de salida indistinta ("+src_dir[0]+")\n\n", 
		    		"2)Prioridad a la salida 2 ("+src_dir[1]+")\n\n", 
		    		"3)Orden de llenado PB-P1("+src_dir[2]+")\n\n" };
		    
		    String input = (String) JOptionPane.showInputDialog(null, "Choose now...",
		        "Choose loading path", JOptionPane.QUESTION_MESSAGE, null, 
		        choices, // Array of choices
		        choices[0]); // Initial choice
		   
		dir = input.charAt(0)-49;
		
		System.out.println();
		
		int Nshots=-1;//eleccion cantidad de  disparos a ejecutar
		while(Nshots< 1) {
		
		    input = (String) JOptionPane.showInputDialog("Ingrese el Numero de disaros a ejecutar"); 

		Nshots = Integer.parseInt(input);
		System.out.println();
			}
		
		boolean printPinv=false;//eleccion para la impresion de pinvariantes
			
			 int n = JOptionPane.showConfirmDialog(
			            null,
			            "Imprimir Invariantes de Plaza por disparo?",
			            "Pinvariants",
			            JOptionPane.YES_NO_OPTION);

			 
			System.out.println();
			
		if (n==0)printPinv=true;
		
		
		/*Lectura de la matriz de incidencia(posterior y anterior),inhibicion,
		marcado de la red,info plazas e info de transiciones.*/
		Lector.parseHtml(src_dir[dir]+"Incidence&Marking.html");
		Lector.parseInvariants(src_dir[dir]+"Invariants.html");
		
		Matriz Incidencia_Posterior=new Matriz(Lector.getIncidenciaF());
		Matriz Incidencia_Anterior=new Matriz(Lector.getIncidenciaB());
		
		Matriz Inhib=new Matriz(Lector.getInhibicion()).getTranspuesta();
		
		int [] Marcado=new Matriz(Lector.getMarcado()).getFila(0);
		
		String[] pinfo=new String[Lector.getpInfo().size()];
		pinfo=Lector.getpInfo().toArray(pinfo);
		
		String[] tinfo=new String[Lector.gettInfo().size()];
		tinfo=Lector.gettInfo().toArray(tinfo);
		
		int[][] Pinvariants = Lector.getpInvariants();
		int [] Pvalues = Lector.getpInvariantValues();
		
		ArrayList<Integer>[] Tinvariants = Lector.getTinvariants();
		
		//Leo y guardo los tiempos y prioridades
		Matriz Tiempos = new Matriz(src_dir[dir]+"Tiempos.txt");
		int[] Prioridades= new Matriz(src_dir[dir]+"Prioridades.txt").getColumna(0);
		Politicas.getInstance(Prioridades);
		Matriz Incidencia_lector=new Matriz(src_dir[dir]+"Lector_matrix.txt");
			
		File path = new File(initial_path+"log.txt");
		Log.getInstance(path);
		
		Log.prints("Programacion concurrente : Trabajo Practico Integrador \n\nCargando red de petri de " + src_dir[dir] + "\n\n");
		
		//Creo la red con los datos leidos
		Monitor.getInstance(RedDePetri.getInstance(Incidencia_Anterior, Incidencia_Posterior, Inhib,Incidencia_lector, Marcado, 
				Tiempos,pinfo,tinfo,Pinvariants,Pvalues,Tinvariants,printPinv,Nshots));
		
		Thread hilo1=new Thread(new Task(new int[]{3}));threads.add(hilo1);//entrada 1
		Thread hilo2=new Thread(new Task(new int[]{4}));threads.add(hilo2);//entrada 2
		Thread hilo3=new Thread(new Task(new int[]{5}));threads.add(hilo3);//entrada 3
		Thread hilo4=new Thread(new Task(new int[]{10}));threads.add(hilo4);//barrera 1
		Thread hilo5=new Thread(new Task(new int[]{6}));threads.add(hilo5);//barrera 2
		Thread hilo6=new Thread(new Task(new int[]{7}));threads.add(hilo6);////barrera 3
		Thread hilo7=new Thread(new Task(new int[]{14}));threads.add(hilo7);//indica ir a planta baja
		Thread hilo8=new Thread(new Task(new int[]{13}));threads.add(hilo8);//indica ir al piso 1
		Thread hilo9=new Thread(new Task(new int[]{17,23,20}));threads.add(hilo9);//sube por la rampa y estaciona en p1
		Thread hilo10=new Thread(new Task(new int[]{24,18,15}));threads.add(hilo10);//se retira de p1 y baja por la rampa
		Thread hilo11=new Thread(new Task(new int[]{25}));threads.add(hilo11);//se retira de su lugar en pb
		Thread hilo12=new Thread(new Task(new int[]{19}));threads.add(hilo12);//vehiculo estaciona en pb
		Thread hilo13=new Thread(new Task(new int[]{21}));threads.add(hilo13);//se retira por s1
		Thread hilo14=new Thread(new Task(new int[]{22}));threads.add(hilo14);//se retira por s2
		Thread hilo15=new Thread(new Task(new int[]{11,1,8}));threads.add(hilo15);//salida1
		Thread hilo16=new Thread(new Task(new int[]{12,2,9}));threads.add(hilo16);//salida 2
		Thread hilo17=new Thread(new Task(new int[]{0,16}));threads.add(hilo17);//cartel

			
		for(int i = threads.size()-1; i >= 0; i--)
			threads.get(i).start();
		
		final JScrollPane jsp = new JScrollPane();
		JFrame f = new JFrame("Executing...");
		jsp.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		  f.add(jsp);
		  f.pack();
		  f.setLocationRelativeTo(null);
	        f.setVisible(true);
	        f.setSize(300,200);;


		
		for(int i = 1; i < threads.size(); i++)
			threads.get(i).join();
		
		f.dispose();
		  Cursor.getDefaultCursor();
		print_stadistics(font);		
		
		Process p = Runtime.getRuntime().exec("python3 Regex.py");
	}
	
	
	
	private static void print_stadistics(Font font) throws FileNotFoundException
	{
		Log.prints(String.format("Simulacion finalizada en %d milisegundos\n\n",RedDePetri.getTiempoEjecucion()));
		RedDePetri.printTshots();
		RedDePetri.printAvgTimes();
		
		Log.prints("----- Analisis de Invariantes de Transicion -----\n\n");
		long now= System.currentTimeMillis();
		RedDePetri.checkTinvariants();
		Log.prints("\n\n-----------------Disparos restantes------------------\n\n");
		RedDePetri.printTshots();
		Log.prints(String.format("Analisis finalizado en %d milisegundos\n\n",System.currentTimeMillis()-now));
		 JTextArea textArea = new JTextArea(Log.getlog(), 45,  110);
		 textArea.setFont( font);
		JScrollPane j = new JScrollPane(textArea);
		JOptionPane.showMessageDialog(null,j);
		Log.printAll();
	}
	
	
}
