package estacionamiento;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class Matriz {
	
	private int matriz [][];
	private int Nfilas=0;
	private int Ncolumnas=0;
	
	public Matriz(String directorio) throws Exception
	{
		cargar(directorio);
	}
	
	//Funcion de carga a traves de un array bidimensional
	public Matriz(int [][] m) throws RuntimeException{
		if(m==null)throw new RuntimeException("Error al cargar matriz : matriz nula");
		Ncolumnas=m[0].length;
		Nfilas=m.length;
		matriz=m; 
	}
	
	public Matriz(int[] v) throws RuntimeException{
		if(v==null)throw new RuntimeException("Error al cargar Vectorr : vector nulo");
		matriz=new int[v.length][1];
		Nfilas=v.length;
		Ncolumnas=1;
	for(int i=0;i<v.length;i++)matriz[i][0]=v[i];	
	}
	
	public void cargar(String directorio) throws FileNotFoundException 
	{
		 Scanner scan = new Scanner(new BufferedReader(new FileReader(directorio)));
		 //Checkeo que el archivo contenga una matriz valida y averiguio sus dimensiones
		 while(scan.hasNextLine()) {
		        Nfilas++;
	        	String[] line = scan.nextLine().trim().split(" ");
	        	if(Nfilas==1)Ncolumnas=line.length;
	        	else if (line.length!=Ncolumnas) {System.out.println("Matriz invalida");scan.close();return;}
	      }
		 
		 if(Nfilas==0 || Ncolumnas==0) {
			 scan.close();
			 throw new RuntimeException("Error al leer la matriz : matriz nula");}
		 
		 scan = new Scanner(new BufferedReader(new FileReader(directorio)));
		 //Cargo la matriz
	      matriz = new int[Nfilas][Ncolumnas];
	      while(scan.hasNextLine()) {
	         for (int i=0; i<matriz.length; i++) {
	            String[] line = scan.nextLine().trim().split(" ");
	            for (int j=0; j<line.length; j++) {
	               matriz[i][j] = Integer.parseInt(line[j]);
	            }
	         }
	      }
	      scan.close();
	}
	
	public void imprimir()
	{
		int l=0;
		for(int i=0;i<matriz.length;i++)
		{
			for(int j=0;j<matriz[0].length;j++) {
				int x=String.valueOf(matriz[i][j]).length();
				if(x>l)l=x;
			}
		}
		l=l+1;		
			for(int i=0;i<matriz.length;i++)
			{
				Log.prints("\n");
				for(int j=0;j<matriz[0].length;j++) {
					Log.prints(String.format("%"+l+"d",matriz[i][j]));
				}
			}
			Log.prints("\n\n\n");
			
	}
	
	public int getNfilas()
	{
		return Nfilas;
	}
	
	public int getNcolumnas()
	{
		return Ncolumnas;
	}
	
	public int[] getColumna(int Ncolumna)
	{
		if(Ncolumna>Ncolumnas || Ncolumna<0) throw new RuntimeException("Numero de columna invalido");
		int[] columna=new int[Nfilas];
		for(int i=0;i<Nfilas;i++) 
		{
			columna[i]=matriz[i][Ncolumna];
		}
		return columna;
	}
	
	public int[] getFila(int nFila) {
		if(nFila>Nfilas|| nFila<0) throw new RuntimeException("Numero de fila invalido");
		int[] fila=new int[Ncolumnas];
		for(int i=0;i<Ncolumnas;i++) 
		{
			fila[i]=matriz[nFila][i];
		}
		return fila;
	}
	
	public int[][] get()
	{
		return matriz;
	}
	

    public Matriz multiplicar(Matriz b){
        int m1 = Nfilas;      
        int n1 = Ncolumnas;   
        int m2 = b.Nfilas;     
        int n2 = b.Ncolumnas;
        if (n1 != m2) throw new RuntimeException("Dimensiones invalidas");
        int[][] c = new int[m1][n2];
        for (int i = 0; i < m1; i++)
            for (int j = 0; j < n2; j++)
                for (int k = 0; k < n1; k++)
                    c[i][j] += matriz[i][k] * b.matriz[k][j];
        return new Matriz(c);

    }
    
    public Matriz multiplicar2(Matriz b){
        int m1 = Nfilas;      
        int n1 = Ncolumnas;   
        int m2 = b.Nfilas;     
        int n2 = b.Ncolumnas;
        if (n1 != m2) throw new RuntimeException("Dimensiones invalidas");
        int[][] c = new int[m1][n2];
        for (int i = 0; i < m1; i++)
            for (int j = 0; j < n2; j++)
                for (int k = 0; k < n1; k++) {
                	if(matriz[i][k]==0 || b.matriz[k][j]==1)c[i][j]=1;
                	else {c[i][j]=0;k=n1;}
                }
        return new Matriz(c);

    }
    
    public Matriz getTranspuesta()
    {
    	int[][] T=new int[Ncolumnas][Nfilas];
    	for(int j=0;j<Nfilas;j++) {
    		for(int i=0;i<Ncolumnas;i++)T[i][j]=matriz[j][i];}
    	return new Matriz(T);
    }

    public Matriz sumar( Matriz b) {
    	if (Nfilas!=b.Nfilas || Ncolumnas!=b.Ncolumnas)throw new RuntimeException("Dimensiones invalidas");
    	int m = Nfilas; 
        int n = b.Ncolumnas;   
        int[][] c = new int[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                    c[i][j] = matriz[i][j] + b.matriz[i][j]  ;
        return new Matriz(c);
    }
    
    public int getNotZero()
    {
    	int k=0;
    	for(int i=0;i<Nfilas;i++) {
    		for(int j=0;j<Ncolumnas;j++) 
    	if(matriz[i][j]!=0)k++;}
    	return k;
    }
    
    public Matriz and(Matriz b)
    {
    	if (Nfilas!=b.Nfilas || Ncolumnas!=b.Ncolumnas)throw new RuntimeException("Dimensiones invalidas");
    	int[][] c = new int[Nfilas][Ncolumnas];
    	for(int i=0;i<Nfilas;i++) {
    		for(int j=0;j<Ncolumnas;j++) {
    			if(matriz[i][j]*b.matriz[i][j]>0)c[i][j]=1;
    			else c[i][j]=0;
    		}
    	}
    	 return new Matriz(c);
    }
    
    public Matriz getIdentity()
    {
    	int[][] c = new int[Nfilas][Nfilas];
    	for(int i=0;i<Nfilas;i++) 
    		for(int j=0;j<Nfilas;j++) {
    			if(i==j)c[i][j]=1;
    			else c[i][j]=0;
    		}
    return new Matriz(c);
    }
    
	public int Column_sum(int column)
	{
		if(column>Ncolumnas)throw new RuntimeException("Invalid column");
		int sum=0;
		for(int i=0;i<Nfilas;i++)sum+=matriz[i][column];
		return sum;
	}
    
    public Matriz restar(Matriz b) 
    {
    	if (Nfilas!=b.Nfilas || Ncolumnas!=b.Ncolumnas)throw new RuntimeException("Dimensiones invalidas");
    	int m = Nfilas; 
        int n = b.Ncolumnas;   
        int[][] c = new int[m][n];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                    c[i][j] = matriz[i][j] - b.matriz[i][j]  ;
        return new Matriz(c);
    }
}
