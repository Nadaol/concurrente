package estacionamiento;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Log extends PrintWriter{
	
	private static Log unique;
	private static String log="";
	private static String firings="";
	
	public Log(File file) throws FileNotFoundException {
		super(file);
	}

	public Log(String file) throws FileNotFoundException {
		super(file);
	}
	
    
    public static Log getInstance(File file) throws FileNotFoundException
    {
    	if(unique==null)unique=new Log(file);
    	return unique;
    }
    
    public static Log getInstance(String file) throws FileNotFoundException
    {
    	if(unique==null)unique=new Log(file);
    	return unique;
    }
    
    public static Log getInstance() 
    {
    	return unique;
    }
    public static String getlog() 
    {
    	return log;
    }
    
	    public static void prints(String x)
	    {
          System.out.print(x);
	      log+=x;
	        }
	    
	    public static void addfiring(int x)
	    {
	    	firings+="T"+Integer.toString(x)+"-";
	    }
	    
	    public static void printAll() throws FileNotFoundException
	    {
	    	unique.print(log);
	    	unique.flush();
	    	unique.close();
	    	PrintWriter pw=new PrintWriter("firings.txt");
	    	pw.print(firings);
	    	pw.flush();
	    	pw.close();
	    }
	  
}
