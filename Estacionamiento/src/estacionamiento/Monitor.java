package estacionamiento;

import java.util.concurrent.Semaphore;

public class Monitor{
    private static Semaphore mutex;// semaforo de disparo
    private static Monitor unique;

    private Monitor(RedDePetri rdp){//Inicializacion del monitor
    	if(rdp==null)throw new RuntimeException("Red de petri no inicializada");
        mutex = new Semaphore(1,true);
      Colas.getInstance();
    }
    
    public static Monitor getInstance(RedDePetri rdp)
    {
    	if(unique==null)unique=new Monitor(rdp);
    	return unique;
    }

    public static void Disparar(int NTrans) throws InterruptedException{
    	 mutex.acquire();
    	 
         while(!RedDePetri.isFinished() && !RedDePetri.disparo(NTrans))
         {	        

        	 mutex.release();
        	 //si la transicion es con tiempo y esta sensibilizada
        	 if(RedDePetri.Temporal_Sensibilizada(NTrans))
        	 {
        		 Thread.sleep(RedDePetri.getVentana(NTrans));
        		 mutex.acquire();
        	 }
        	 else 
        	 {
        	 Colas.adquireT(NTrans);   	
        	 mutex.acquire();
        	 }
          	}
         if(RedDePetri.isFinished()) {
    		 if(Colas.quienesEstan().getNotZero()>0)Colas.releaseAll();
    		 mutex.release();
    	 }
         Matriz m = RedDePetri.getSensibilizadas().and(Colas.quienesEstan());
       
			if(m.getNotZero()>1)
			{	
				int T=Politicas.cual();
          	Colas.releaseT(T);
			}
			  mutex.release();
     }
    }
       
