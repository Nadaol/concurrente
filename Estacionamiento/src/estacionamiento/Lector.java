package estacionamiento;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Lector {
	private static int[][] incidenciaF;
	private static int[][] incidenciaB;
	private static int[][] marcado;
	private static int[][] inhibicion;
	private static int[][]	tInvariants;
	private static int[][] pInvariants;
	private static int[] pInvariantValues;
			
	private static List<String> pInfo=new ArrayList<>();//Encabezados
	private static List<String> tInfo=new ArrayList<>();
	private static List<String> PinvInfo=new ArrayList<>();
	private static List<String> TinvInfo=new ArrayList<>();
	

	public static void parseInvariants(String source){
		try {
			Document doc=Jsoup.parse(new File(source), "utf-8");
			Elements headers=doc.select("h3");
			Elements tables=doc.select("table");
//			File file = new File("/home/martin/.txt");
//			FileWriter fr = new FileWriter(file, false);
			for(int t=0;t<tables.size();t++){
				Element current=tables.get(t);
				Elements rows=current.select("tr");
				int colSize=rows.get(0).select("td").size();
				int[][] auxArray=new int[rows.size()-1][colSize];
				
//				fr.write("\n"+t+"nueva tabla\n\n");
				//valor primera fila es el nombre de columna
				
				for(int i=0;i<rows.size();i++){
					Element currentRow=rows.get(i);
					Elements cols=currentRow.select("td");
//					fr.write("\n");
					
					for(int j=0;j<cols.size();j++){
						if(i==0 && headers.get(t).text().contains("P-Invariants")){
							PinvInfo.add(cols.get(j).text());
						}
						else if(i==0 && headers.get(t).text().contains("T-Invariants"))TinvInfo.add(cols.get(j).text());
						else if(i>0){
							if(!(cols.get(j).text().contains("yes")|| cols.get(j).text().contains("no"))){
								auxArray[i-1][j]=Integer.parseInt(cols.get(j).text());
//								fr.write(cols.get(j).text()+" ");
							}
						}
						
					}
				}
				
								
				if(headers.get(t).text().contains("T-Invariants")){
					tInvariants=new int[auxArray.length][colSize];
					tInvariants=auxArray;
				}else if(headers.get(t).text().contains("P-Invariants")){
					pInvariants=new int[auxArray.length][colSize];
					pInvariants=auxArray;
				}
			}
			String[] auxText=doc.select("body").text().split("P-Invariant equations");
			String[] ecuaciones=auxText[1].split("=");
			pInvariantValues=new int[ecuaciones.length-1];
			for(int k=1;k<ecuaciones.length;k++){
				String[] aux5=ecuaciones[k].split(" ");
				pInvariantValues[k-1]=Integer.parseInt(aux5[1]);			
			}
//			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	//Lectura de archivo,retorna vector de strings por linea
		public static String[] getVector(String read_dir) throws FileNotFoundException
		{
			ArrayList<String>vector=new ArrayList<String>();
			 Scanner scan = new Scanner(new BufferedReader(new FileReader(read_dir)));
			 //Cargo la matriz
		      while(scan.hasNextLine()) {
		    	  vector.add(scan.nextLine());
		      }
		      String[]s = new String[vector.size()];
		      for(int i=0;i<vector.size();i++)
		      {
		    	  s[i]=vector.get(i);
		      }
		      scan.close();
		      return s;
		}

	
	public static void parseHtml(String source){
		try {
			Document doc=Jsoup.parse(new File(source), "utf-8");
			Elements headers=doc.select("table .colhead:only-child");
			Elements tablesValues=doc.select("table tr table");
//			File file = new File("/home/martin/.txt");
//			FileWriter fr = new FileWriter(file, false);
			for(int t=0;t<tablesValues.size();t++){
				Element current=tablesValues.get(t);
				Elements rows=current.select("tr");
				int colSize=rows.get(0).select("td").size();
				int[][] auxArray=new int[rows.size()-1][colSize-1];
				
//				fr.write("\n"+t+"nueva tabla\n\n");
				//valor primera fila es el nombre de columna
				
				for(int i=0;i<rows.size();i++){
					Element currentRow=rows.get(i);
					Elements cols=currentRow.select("td");
//					fr.write("\n");
					//valor primera columna es el nombre de la fila
					if(headers.get(t).text().contains("Forwards incidence matrix")){
						if(i>0)pInfo.add(cols.get(0).text());
					}
					
					for(int j=1;j<cols.size();j++){
						if(i==0 && headers.get(t).text().contains("Forwards incidence matrix")){
							tInfo.add(cols.get(j).text());
						}else if(i>0){
							if(!(cols.get(j).text().contains("yes")|| cols.get(j).text().contains("no"))){
								auxArray[i-1][j-1]=Integer.parseInt(cols.get(j).text());
//								fr.write(cols.get(j).text()+" ");
							}
						}
						
					}
				}
				
				if(headers.get(t).text().contains("Forwards incidence matrix")){
					incidenciaF=new int[auxArray.length][colSize];
					incidenciaF=auxArray;
				}else if(headers.get(t).text().contains("Backwards incidence matrix")){
					incidenciaB=new int[auxArray.length][colSize];
					incidenciaB=auxArray;
				}else if(headers.get(t).text().contains("Inhibition matrix")){
					inhibicion=new int[auxArray.length][colSize];
					inhibicion=auxArray;
				}else if(headers.get(t).text().contains("Marking")){
					marcado=new int[auxArray.length][colSize];
					marcado=auxArray;
				}
			}
//			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

	public static int[][] getIncidenciaF() {
		return incidenciaF;
	}

	public static int[][] getIncidenciaB() {
		return incidenciaB;
	}

	public static int[][] getMarcado() {
		return marcado;
	}

	public static int[][] getInhibicion() {
		return inhibicion;
	}

	public static void setIncidenciaF(int[][] incidenciaF) {
		Lector.incidenciaF = incidenciaF;
	}

	public static void setIncidenciaB(int[][] incidenciaB) {
		Lector.incidenciaB = incidenciaB;
	}

	public static void setMarcado(int[][] marcado) {
		Lector.marcado = marcado;
	}

	public static void setInhibicion(int[][] inhibicion) {
		Lector.inhibicion = inhibicion;
	}

	public static List<String> getpInfo() {
		return pInfo;
	}

	public static void setpInfo(List<String> pInfo) {
		Lector.pInfo = pInfo;
	}

	public static List<String> gettInfo() {
		return tInfo;
	}

	public static void settInfo(List<String> tInfo) {
		Lector.tInfo = tInfo;
	}

	public static ArrayList<Integer>[] getTinvariants()
	{
		// E1 (3,10) E2 (4,6) E3(5,7) S1(21,11,1,8) S2(22,12,2,9) P1(13,17,23,20,24,18,15) PB(14,19,25)
		ArrayList<Integer>[] tinv = new ArrayList[13];
		tinv[0]=new ArrayList<Integer>( Arrays.asList(3,10,14,19,25,21,11,1,8));//"E1 PB S1"
		tinv[1]=new ArrayList<Integer>( Arrays.asList(4,6,14,19,25,21,11,1,8));//"E2 PB S1"
		tinv[2]=new ArrayList<Integer>( Arrays.asList(5,7,14,19,25,21,11,1,8));//"E3 PB S1"
		tinv[3]=new ArrayList<Integer>( Arrays.asList(3,10,13,17,23,20,24,18,15,21,11,1,8));//"E1 P1 S1"
		tinv[4]=new ArrayList<Integer>( Arrays.asList(4,6,13,17,23,20,24,18,15,21,11,1,8));//"E2 P1 S1"
		tinv[5]=new ArrayList<Integer>( Arrays.asList(5,7,13,17,23,20,24,18,15,21,11,1,8));//"E3 P1 S1"
		tinv[6]=new ArrayList<Integer>( Arrays.asList(3,10,14,19,25,22,12,2,9));//"E1 PB S2"
		tinv[7]=new ArrayList<Integer>( Arrays.asList(4,6,14,19,25,22,12,2,9));//"E2 PB S2"
		tinv[8]=new ArrayList<Integer>( Arrays.asList(5,7,14,19,25,22,12,2,9));//"E3 PB S2"
		tinv[9]=new ArrayList<Integer>( Arrays.asList(3,10,13,17,23,20,24,18,15,22,12,2,9));//"E1 P1 S2"
		tinv[10]=new ArrayList<Integer>( Arrays.asList(4,6,13,17,23,20,24,18,15,22,12,2,9));//"E2 P1 S2"
		tinv[11]=new ArrayList<Integer>( Arrays.asList(5,7,13,17,23,20,24,18,15,22,12,2,9));//"E3 P1 S2"
		tinv[12]=new ArrayList<Integer>( Arrays.asList(0,16));//"CARTEL"
		return tinv;
	}
	
	public static void settInvariants(int[][] tInvariants) {
		Lector.tInvariants = tInvariants;
	}

	public static int[][] getpInvariants() {
		return pInvariants;
	}

	public static void setpInvariants(int[][] pInvariants) {
		Lector.pInvariants = pInvariants;
	}

	public static int[] getpInvariantValues() {
		return pInvariantValues;
	}

	public static void setpInvariantValues(int[] pInvariantValues) {
		Lector.pInvariantValues = pInvariantValues;
	}

	public static List <String> getPinvInfo() {
		return PinvInfo;
	}
	
	public static List <String> getTinvInfo() {
		return TinvInfo;
	}
	
}