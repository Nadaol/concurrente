import re
from datetime import datetime

Cycles = [0] * 13
T = [""] * 13
i = 0                                               #contador de invariantes
firings = open('firings.txt', 'r')                  #log de lectura de disparos 
log=""                                              #log de substituciones (invariantes) 
flog = open('Regex_Invariants_results.txt', 'w+')   #resutados de ciclos completados
Tinv = open('Estacionamiento/src/parametros_lectura/Tinvariants.txt')   #lectura invatriantes
time = datetime.now()

#retorna txt sin la primer secuencia detectada por la regex,y la imprime segun los grupos validos(group2) en log
def deleteMatchAndPrint(regex, txt, group1, group2):    
    g = 0
    global i, log
    salidas=re.compile('(T8-)|(T9-)')
    match = salidas.search(txt)
    if match is None:
        return None
    i = i + 1
    match=regex.search(txt)
    for g in range(len(group2)):
        if match[group2[g]] is not None:
            log=log+str((match[group2[g]]))
    log=log+("\n")
    return regex.sub(group1, txt, count=1)

#lectura de archivo de disparos e invariantes
line = Tinv.readline()
cnt=0
while line:
    T[cnt]=line.strip('\n')
    line = Tinv.readline()
    cnt=cnt+1
cadena = firings.read()

#compila la expresion regular E1|E2|E3 + PB|P1 + S1|S2 
paths = re.compile(
    '((((T3-)(.*?)(T10-))|((T4-)(.*?)(T6-))|((T5-)(.*?)(T7-)))(.*?)(((T14-)(.*?)(T19-)(.*?)(T25-))|((T13-)(.*?)(T17-)(.*?)(T23-)(.*?)(T20-)(.*?)(T24-)(.*?)(T18-)(.*?)(T15-)))(.*?)(((T21-)(.*?)(T11-)(.*?)(T1-)(.*?)(T8-))|((T22-)(.*?)(T12-)(.*?)(T2-)(.*?)(T9-))))')
#expresion regular para el ciclo on/off del cartel
cartel = re.compile('(T0-)(.*?)(T16-)')
#grupos para eliminar(T.-) del log y para imrimir secuencias
stay_groups = r'\5\9\13\15\19\21\25\27\29\31\33\35\37\41\43\45\49\51\53'
rmv_groups = [4, 6, 8, 10, 12, 14, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 40, 42, 44, 46, 48, 50, 52, 54]

#sustituyo todas las secuencias on/off del cartel
j=len(cartel.findall(cadena))
cadena = cartel.sub(r'\2', cadena)

#sustituyo todos los caminos y los imprimo para luego contar los ciclos
while True: 
    cadena_aux = deleteMatchAndPrint(paths, cadena, stay_groups, rmv_groups)
    if cadena_aux is not None:
        cadena = cadena_aux
    else:
        break

#cuento los invariantes impresos en el log
for line in log.split('\n'):
    h = 0
    while h < len(T):
        if (line == T[h]):
            Cycles[h] = Cycles[h] + 1
        h = h + 1
Cycles[12]=j
h=0

#imprimo resultados finales al archivo de salida
while h in range(len(Cycles)):
    flog.write(T[h] + " : " + str(Cycles[h])+"\n")
    h = h + 1

time=datetime.now()-time
flog.write('Tiempo de ejecucion del analisis : '+str(time))
